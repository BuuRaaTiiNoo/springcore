package org.shop.appconfig;


import org.shop.*;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


@Configuration
@Import({RepositoryConfig.class, InitializerConfig.class, ServiceConfig.class})
public class AppConfig {
    @Bean
    public BeanPostProcessor displayNameBeanPostProcessor() {
        return new DisplayNameBeanPostProcessor();
    }
}
